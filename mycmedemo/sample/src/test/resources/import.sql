INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('ABCD', 'OPER. MANAGER', true, '', 90000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('BCDE', 'PROGRAMMER I', true, '', 70000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('CDEF', 'PROGRAMMER II', true, '', 80000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('DEFG', 'PROGRAMMER III', true, '', 90000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('EFGH', 'INTEGRATIONS MANAGER', true, '', 89000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('FGHI', 'BUSINESS ANALYST', true, '', 75000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('HIJK', 'THE MAN WHO SLEEPS', true, '', 10000.00);
INSERT INTO job_position (code, name, is_enabled, description, base_salary) VALUES ('IJKL', 'KEY ACCOUNT MANAGER', true, '', 200000.00);

INSERT INTO employee (doc_id, first_name, mid_name, last_name, position)  VALUES ('95555552', 'Gandalf', null, 'Stormcrow', 'BCDE');
INSERT INTO employee (doc_id, first_name, mid_name, last_name, position)  VALUES ('24444452', 'Beren', null, 'Erchamion', 'EFGH');
INSERT INTO employee (doc_id, first_name, mid_name, last_name, position)  VALUES ('22345674', 'Daenerys', 'Stormborn', 'Targaryen', 'IJKL');
INSERT INTO employee (doc_id, first_name, mid_name, last_name, position)  VALUES ('97754443', 'King', null, 'Aslan', 'FGHI');
INSERT INTO employee (doc_id, first_name, mid_name, last_name, position)  VALUES ('24467789', 'Metatron', null, 'Shekina', 'EFGH');
