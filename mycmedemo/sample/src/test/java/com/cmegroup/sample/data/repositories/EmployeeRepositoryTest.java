package com.cmegroup.sample.data.repositories;

import java.util.Collection;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cmegroup.sample.H2JpaConfig;
import com.cmegroup.sample.data.entities.Employee;
import com.cmegroup.sample.data.repositories.specs.EmployeeSpecification;
import com.cmegroup.sample.dto.EmployeeSearchDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { H2JpaConfig.class })
@ActiveProfiles(profiles = { "test" })
public class EmployeeRepositoryTest {

	@Autowired
	private EmployeeRepository repository;

	@Test
	@Transactional
	public void givenRepository_WhenFindAllValidateNotEmpty_ThenOk() {
		// Results found
		Collection<Employee> resultA = repository.findAll();
		Assert.assertTrue(!resultA.isEmpty());
		// Results by search criteria found
		EmployeeSearchDTO dto1 = new EmployeeSearchDTO();
		dto1.setFirstName("Daenerys");
		dto1.setLastName("Stormcrow");
		Collection<Employee> resultB = repository.findAll(EmployeeSpecification.hasCriteria(dto1, false));
		Assert.assertTrue(!resultB.isEmpty());
		Assert.assertEquals(2L, resultB.size());
		// Results by mutually-excluding search criteria found
		EmployeeSearchDTO dto2 = new EmployeeSearchDTO();
		dto2.setFirstName("Daenerys");
		dto2.setMiddleName("Stormborn");
		Collection<Employee> resultC = repository.findAll(EmployeeSpecification.hasCriteria(dto2, true));
		Assert.assertTrue(!resultC.isEmpty());
		Assert.assertEquals(1L, resultC.size());
	}

	@Test
	@Transactional
	public void givenRepository_WhenFindByIdValidateHasPosition_ThenOk() {
		// This will help to confirm if the lazy loading of this entity is working fine
		EmployeeSearchDTO dto1 = new EmployeeSearchDTO();
		dto1.setFirstName("Daenerys");
		Collection<Employee> resultA = repository.findAll(EmployeeSpecification.hasCriteria(dto1, true));
		Assert.assertTrue(!resultA.isEmpty());
		Optional<Employee> employee = resultA.stream().findAny();
		Assert.assertTrue(employee.isPresent());
		Assert.assertNotNull(employee.get().getCurrentPosition());
		Assert.assertTrue(!employee.get().getCurrentPosition().getCode().isEmpty());
	}

	@Test
	@Transactional
	public void givenRepository_WhenFindAllValidateEmpty_ThenOk() {
		// Results by mutually-excluding search criteria not found
		EmployeeSearchDTO dto1 = new EmployeeSearchDTO();
		dto1.setFirstName("Gandalf");
		dto1.setLastName("Targaryen");
		Collection<Employee> resultB = repository.findAll(EmployeeSpecification.hasCriteria(dto1, true));
		Assert.assertTrue(resultB.isEmpty());
		// Results by search criteria not found
		EmployeeSearchDTO dto2 = new EmployeeSearchDTO();
		dto2.setMiddleName("Manepally");
		dto2.setLastName("Elisa");
		Collection<Employee> resultC = repository.findAll(EmployeeSpecification.hasCriteria(dto2, true));
		Assert.assertTrue(resultC.isEmpty());
	}

	@Test
	public void givenRepository_WhenFindByDocIdValidateNotNull_ThenOk() {
		Optional<Employee> result = repository.findByDocId("97754443");
		Assert.assertTrue(result.isPresent());
		Assert.assertEquals(result.get().getFirstName(), "King");
	}

}
