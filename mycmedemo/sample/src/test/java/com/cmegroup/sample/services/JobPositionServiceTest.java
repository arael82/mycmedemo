package com.cmegroup.sample.services;

import java.math.BigDecimal;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.cmegroup.sample.H2JpaConfig;
import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.data.repositories.JobPositionRepository;
import com.cmegroup.sample.dto.JobPositionDTO;
import com.cmegroup.sample.dto.JobPositionSearchDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.exceptions.PreconditionFailedException;
import com.cmegroup.sample.utils.RandomTextUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { H2JpaConfig.class })
@ActiveProfiles(profiles = { "test" })
public class JobPositionServiceTest {

	@Autowired
	private JobPositionService service;

	@Autowired
	private JobPositionRepository repository;

	@Test
	public void givenService_WhenFindValidatesuccess_ThenOk() {
		// Results found
		Collection<JobPositionDTO> resultA = service.find(new JobPositionSearchDTO());
		Assert.assertTrue(!resultA.isEmpty());
		// Results by search criteria found
		JobPositionSearchDTO dto1 = new JobPositionSearchDTO();
		dto1.setName("PROGRAMMER");
		dto1.setBaseSalaryFrom(new BigDecimal("10000.00"));
		dto1.setBaseSalaryTo(new BigDecimal("75000"));
		Collection<JobPositionDTO> resultB = service.find(dto1);
		Assert.assertTrue(!resultB.isEmpty());
		Assert.assertEquals(1L, resultB.size());
	}


	@Test(expected = NotFoundException.class)
	public void givenService_WhenFindValidateNotFound_ThenThrowException() {
		// Results by search criteria found
		JobPositionSearchDTO dto1 = new JobPositionSearchDTO();
		dto1.setName("NOWAY");
		service.find(dto1);
	}

	@Test
	public void givenService_WhenCreateValidateSuccess_ThenOk() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		Assert.assertEquals(newlyCreatedPosition.getBaseSalary(), newJobPosition.getBaseSalary());
	}

	@Test(expected = AlreadyExistsException.class)
	public void givenService_WhenCreateValidateAlreadyExists_ThenThrowException() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		Assert.assertEquals(newlyCreatedPosition.getBaseSalary(), newJobPosition.getBaseSalary());
		// Let us try to add the record it will throw an exception.
		service.create(newJobPosition);
	}

	@Test(expected = PreconditionFailedException.class)
	public void givenService_WhenCreateValidatePreconditionFailed_ThenThrowException() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		newJobPosition.setName(null);
		service.create(newJobPosition);
	}

	@Test
	public void givenService_WhenReadValidateNotNull_ThenOk() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		Assert.assertEquals(newlyCreatedPosition.getBaseSalary(), newJobPosition.getBaseSalary());
		// Let us check that the record exists
		JobPositionDTO found = service.read(newlyCreatedPosition.getCode());
		Assert.assertEquals(found.getCode(), newlyCreatedPosition.getCode());
		Assert.assertEquals(found.getName(), newlyCreatedPosition.getName());
	}

	@Test(expected = NotFoundException.class)
	public void givenService_WhenReadValidateNull_ThenThrowException() {
		service.read("XXXX");
	}

	@Test
	public void givenService_WhenUpdateValidateSuccess_ThenOk() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		// Let us try to edit the record
		newlyCreatedPosition.setName(RandomTextUtil.randomString(50));
		JobPositionDTO updatedPosition = service.update(newlyCreatedPosition);
		Assert.assertNotEquals(updatedPosition.getName(), newJobPosition.getName());
	}

	@Test(expected = NotFoundException.class)
	public void givenService_WhenUpdateValidateNotFound_ThenThrowException() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		// Let us try to edit the record (by changing the code to another that does not
		// exist)
		newlyCreatedPosition.setCode("qzzz");
		service.update(newlyCreatedPosition);
	}

	@Test(expected = PreconditionFailedException.class)
	public void givenService_WhenUpdateValidatePreconditionFailed_ThenThrowException() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		// Let us try to edit the record (by changing the code to another that does not
		// exist)
		newlyCreatedPosition.setName(null);
		service.update(newlyCreatedPosition);
	}

	@Test
	public void givenService_WhenDeleteValidateSuccess_ThenOk() {
		JobPositionDTO newJobPosition = this.createTestPositionDTO(BigDecimal.valueOf(15000));
		JobPositionDTO newlyCreatedPosition = service.create(newJobPosition);
		Assert.assertEquals(newlyCreatedPosition.getCode(), newJobPosition.getCode());
		Assert.assertEquals(newlyCreatedPosition.getName(), newJobPosition.getName());
		// Let us try to edit the record (by changing the code to another that does not
		// exist)
		service.delete(newlyCreatedPosition.getCode());
	}

	@Test(expected = NotFoundException.class)
	public void givenService_WhenDeleteValidateNotFound_ThenThrowException() {
		String code = "ANY";
		service.delete(code);
	}

	@Test(expected = PreconditionFailedException.class)
	public void givenService_WhenDeleteValidatePreconditionFailed_ThenThrowException() {
		String code = "EFGH";
		service.delete(code);
	}

	/**
	 * Just for testing purposes
	 * 
	 * @param baseSalary
	 * @return
	 */
	private String addTestPosition(BigDecimal baseSalary) {
		String code = RandomTextUtil.randomString(4);
		String name = RandomTextUtil.randomString(50);
		JobPosition jobPosition = new JobPosition();
		jobPosition.setBaseSalary(baseSalary);
		jobPosition.setCode(code);
		jobPosition.setName(name);
		repository.save(jobPosition);
		return code;
	}

	/**
	 * Just for testing purposes
	 * 
	 * @param baseSalary
	 * @return
	 */
	private JobPositionDTO createTestPositionDTO(BigDecimal baseSalary) {
		String code = RandomTextUtil.randomString(4);
		String name = RandomTextUtil.randomString(50);
		JobPositionDTO jobPosition = new JobPositionDTO();
		jobPosition.setBaseSalary(baseSalary);
		jobPosition.setCode(code);
		jobPosition.setName(name);
		return jobPosition;

	}
}
