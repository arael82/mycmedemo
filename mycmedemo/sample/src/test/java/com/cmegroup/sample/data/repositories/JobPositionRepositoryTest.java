package com.cmegroup.sample.data.repositories;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cmegroup.sample.H2JpaConfig;
import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.data.repositories.specs.JobPositionSpecification;
import com.cmegroup.sample.dto.JobPositionSearchDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { H2JpaConfig.class })
@ActiveProfiles(profiles = { "test" })
public class JobPositionRepositoryTest {

	@Autowired
	private JobPositionRepository repository;

	@Test
	@Transactional
	public void givenRepository_WhenFindAllValidateNotEmpty_ThenOk() {
		// Results found
		Collection<JobPosition> resultA = repository.findAll();
		Assert.assertTrue(!resultA.isEmpty());
		// Results by search criteria found
		JobPositionSearchDTO dto1 = new JobPositionSearchDTO();
		dto1.setName("NOWAY");
		dto1.setBaseSalaryFrom(new BigDecimal("10000.00"));
		dto1.setBaseSalaryTo(new BigDecimal("75000"));
		Collection<JobPosition> resultB = repository.findAll(JobPositionSpecification.hasCriteria(dto1, false));
		Assert.assertTrue(!resultB.isEmpty());
		Assert.assertEquals(3L, resultB.size());
		// Results by mutually-excluding search criteria found
		JobPositionSearchDTO dto2 = new JobPositionSearchDTO();
		dto2.setName("MANAGER");
		dto2.setBaseSalaryFrom(new BigDecimal("80000"));
		dto2.setBaseSalaryTo(new BigDecimal("89000"));
		Collection<JobPosition> resultC = repository.findAll(JobPositionSpecification.hasCriteria(dto2, true));
		Assert.assertTrue(!resultC.isEmpty());
		Assert.assertEquals(1L, resultC.size());
	}

	@Test
	@Transactional
	public void givenRepository_WhenFindByIdValidateHasEmployees_ThenOk() {
		// This will help to confirm if the lazy loading of this entity is working fine
		Optional<JobPosition> result = repository.findById("EFGH");
		Assert.assertTrue(result.isPresent());
		Assert.assertTrue(!result.get().getEmployees().isEmpty());
		Assert.assertEquals(2L, result.get().getEmployees().size());
	}

	@Test
	@Transactional
	public void givenRepository_WhenFindAllValidateEmpty_ThenOk() {
		// Results by search criteria not found
		JobPositionSearchDTO dto1 = new JobPositionSearchDTO();
		dto1.setName("NOWAY");
		dto1.setBaseSalaryFrom(new BigDecimal("900000.00"));
		dto1.setBaseSalaryTo(new BigDecimal("25000000"));
		Collection<JobPosition> resultB = repository.findAll(JobPositionSpecification.hasCriteria(dto1, false));
		Assert.assertTrue(resultB.isEmpty());
		// Results by mutually-excluding search criteria not found
		JobPositionSearchDTO dto2 = new JobPositionSearchDTO();
		dto2.setName("THE MAN WHO SLEEPS");
		dto2.setBaseSalaryFrom(new BigDecimal("80000"));
		dto2.setBaseSalaryTo(new BigDecimal("89000"));
		Collection<JobPosition> resultC = repository.findAll(JobPositionSpecification.hasCriteria(dto2, true));
		Assert.assertTrue(resultC.isEmpty());
	}

}
