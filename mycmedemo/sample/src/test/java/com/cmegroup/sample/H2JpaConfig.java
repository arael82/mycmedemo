package com.cmegroup.sample;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "com.cmegroup.sample.data.repositories")
@ComponentScan("com.cmegroup.sample")
@EnableTransactionManagement
public class H2JpaConfig {

}
