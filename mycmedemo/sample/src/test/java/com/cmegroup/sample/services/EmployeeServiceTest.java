package com.cmegroup.sample.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.cmegroup.sample.H2JpaConfig;
import com.cmegroup.sample.dto.EmployeeDTO;
import com.cmegroup.sample.dto.EmployeeInputDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.utils.RandomTextUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { H2JpaConfig.class })
@ActiveProfiles(profiles = { "test" })
public class EmployeeServiceTest {

	@Autowired
	private EmployeeService service;

	@Test
	public void givenService_WhenFindValidateNotEmpty_ThenOk() {
	}

	@Test
	public void givenService_WhenFindValidateEmpty_ThenThrowException() {
	}

	@Test
	public void givenService_WhenCreateValidateSuccess_ThenOk() {
		EmployeeInputDTO input = createTestEmployeeDTO();
		EmployeeDTO result = service.create(input);
		Assert.assertNotNull(result);
		Assert.assertNotNull(result.getDocId());
		Assert.assertNotNull(result.getFirstName());
		Assert.assertNotNull(result.getLastName());
		Assert.assertNotNull(result.getCurrentPosition());
		Assert.assertTrue(!result.getCurrentPosition().getCode().isEmpty());
	}

	@Test(expected = AlreadyExistsException.class)
	public void givenService_WhenCreateValidateAlreadyExists_ThenThrowException() {
		EmployeeInputDTO input = createTestEmployeeDTO();
		EmployeeDTO result = service.create(input);
		Assert.assertNotNull(result);
		Assert.assertNotNull(result.getDocId());
		Assert.assertNotNull(result.getFirstName());
		Assert.assertNotNull(result.getLastName());
		Assert.assertNotNull(result.getCurrentPosition());
		Assert.assertTrue(!result.getCurrentPosition().getCode().isEmpty());
		// Now let us try to add it again - will throw exception
		service.create(input);
	}

	@Test
	public void givenService_WhenCreateValidatePreconditionFailed_ThenThrowException() {
	}

	@Test
	public void givenService_WhenReadValidateNotNull_ThenOk() {
		String documentId = "24444452";
		EmployeeDTO result = service.read(documentId);
		Assert.assertNotNull(result);
		Assert.assertEquals(documentId, result.getDocId());
		Assert.assertEquals("Beren", result.getFirstName());
	}

	@Test(expected = NotFoundException.class)
	public void givenService_WhenReadValidateNotFound_ThenThrowException() {
		String documentId = "UNEXISTING";
		service.read(documentId);
	}

	/**
	 * Just for testing purposes
	 * 
	 * @param baseSalary
	 * @return
	 */
	private EmployeeInputDTO createTestEmployeeDTO() {
		String documentID = RandomTextUtil.randomString(20);
		String firstName = RandomTextUtil.randomString(20);
		String lastName = RandomTextUtil.randomString(20);
		String jobPosition = "HIJK";
		return new EmployeeInputDTO(documentID, firstName, "", lastName, jobPosition);

	}
}
