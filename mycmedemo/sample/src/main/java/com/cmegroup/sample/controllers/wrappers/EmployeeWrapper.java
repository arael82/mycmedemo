package com.cmegroup.sample.controllers.wrappers;

import java.io.Serializable;

import com.cmegroup.sample.dto.EmployeeInputDTO;
import com.cmegroup.sample.dto.EmployeeSearchDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeWrapper implements Serializable {

	private static final long serialVersionUID = 5168990057225669574L;
	private EmployeeInputDTO employeeDTO = new EmployeeInputDTO();
	private EmployeeSearchDTO searchCriteria = new EmployeeSearchDTO();

}
