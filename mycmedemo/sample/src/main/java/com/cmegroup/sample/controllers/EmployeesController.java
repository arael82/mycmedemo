package com.cmegroup.sample.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cmegroup.sample.controllers.wrappers.EmployeeWrapper;
import com.cmegroup.sample.controllers.wrappers.JobPositionWrapper;
import com.cmegroup.sample.dto.EmployeeDTO;
import com.cmegroup.sample.dto.EmployeeSearchDTO;
import com.cmegroup.sample.dto.JobPositionDTO;
import com.cmegroup.sample.services.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/employees")
@Slf4j
public class EmployeesController {

	@Autowired
	private EmployeeService service;

	@RequestMapping(value = "search", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Collection<EmployeeDTO>> find(@RequestBody EmployeeWrapper wrapper) {
		return new ResponseEntity<Collection<EmployeeDTO>>(service.find(wrapper.getSearchCriteria()), HttpStatus.OK);
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Collection<EmployeeDTO>> find() {
		return new ResponseEntity<Collection<EmployeeDTO>>(service.find(), HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<EmployeeDTO> create(@RequestBody EmployeeWrapper wrapper) {
		log.info("User has requested creation of record: job position");
		return new ResponseEntity<EmployeeDTO>(service.create(wrapper.getEmployeeDTO()), HttpStatus.OK);
	}

	@RequestMapping(value = "/{documentId}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<EmployeeDTO> read(@PathVariable(name="documentId", required = true) String documentId) {
		return new ResponseEntity<EmployeeDTO>(service.read(documentId), HttpStatus.OK);
	}

}
