package com.cmegroup.sample.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeSearchDTO {

	public Long id;

	public String docId;

	public String firstName;

	public String middleName;

	public String lastName;

	public EmployeeSearchDTO(String docId, String firstName, String middleName, String lastName) {
		super();
		this.docId = docId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}

}
