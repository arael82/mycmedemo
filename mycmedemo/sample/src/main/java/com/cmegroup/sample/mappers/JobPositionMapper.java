package com.cmegroup.sample.mappers;

import java.util.Collection;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.dto.JobPositionDTO;

@Mapper(componentModel = "spring")
public interface JobPositionMapper {

	JobPositionMapper INSTANCE = Mappers.getMapper(JobPositionMapper.class);

	JobPositionDTO toDTO(JobPosition source);

	JobPosition toEntity(JobPositionDTO source);

	Collection<JobPositionDTO> toDTOs(Collection<JobPosition> source);

	Collection<JobPosition> toEntities(Collection<JobPositionDTO> source);

}
