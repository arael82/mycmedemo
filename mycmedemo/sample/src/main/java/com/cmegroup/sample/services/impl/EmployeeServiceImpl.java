package com.cmegroup.sample.services.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmegroup.sample.data.entities.Employee;
import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.data.repositories.EmployeeRepository;
import com.cmegroup.sample.data.repositories.specs.EmployeeSpecification;
import com.cmegroup.sample.dto.EmployeeDTO;
import com.cmegroup.sample.dto.EmployeeInputDTO;
import com.cmegroup.sample.dto.EmployeeSearchDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.exceptions.PreconditionFailedException;
import com.cmegroup.sample.mappers.EmployeeMapper;
import com.cmegroup.sample.mappers.JobPositionMapper;
import com.cmegroup.sample.services.EmployeeService;
import com.cmegroup.sample.services.JobPositionService;

/**
 * Services and main business logic for Employee Service
 * 
 * @author leonardo
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repository;

	@Autowired
	private EmployeeMapper mapper;
	
	@Autowired
	private JobPositionMapper jobPositionMapper;
	
	@Autowired
	private JobPositionService jobPositionService;

	/**
	 * List all the employees by search criteria.
	 * 
	 * @param searchCriteria the search criteria.
	 * @return a collection containing all the employees.
	 * @throws NotFoundException if no employee is found.
	 */
	@Override
	public Collection<EmployeeDTO> find() throws NotFoundException {
		Collection<Employee> result = repository.findAll();
		if (result.isEmpty())
			throw new NotFoundException("No employees were found.");
		return mapper.toDTOs(result).stream().sorted().collect(Collectors.toList());
	}

	/**
	 * List all the employees by search criteria.
	 * 
	 * @param searchCriteria the search criteria.
	 * @return a collection containing all the employees.
	 * @throws NotFoundException if no employee is found.
	 */
	@Override
	public Collection<EmployeeDTO> find(EmployeeSearchDTO searchCriteria) throws NotFoundException {
		Collection<Employee> result = repository.findAll(EmployeeSpecification.hasCriteria(searchCriteria, true));
		if (result.isEmpty())
			throw new NotFoundException("No employees were found with the specified search criteria");
		return mapper.toDTOs(result).stream().sorted().collect(Collectors.toList());
	}

	/**
	 * Creates a new Employee.
	 * 
	 * <p>
	 * This operation will be successful if:
	 * <ul>
	 * <li>All required data is provided: First Name, Last Name, Job Position Code,
	 * Document Id.</li>
	 * <li>The document ID is unique.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param newEmployee the information for the new employee
	 * @return the new just-added employee info.
	 * @throws AlreadyExistsException      if an employee with same document id
	 *                                     exists
	 * @throws PreconditionFailedException if required data is not input correctly.
	 */

	@Override
	public EmployeeDTO create(EmployeeInputDTO newEmployee)
			throws AlreadyExistsException, PreconditionFailedException {

		// Avoid duplicate data
		EmployeeSearchDTO searchDTO = new EmployeeSearchDTO();
		searchDTO.setDocId(newEmployee.getDocId());
		Collection<Employee> duplicates = repository.findAll(EmployeeSpecification.hasCriteria(searchDTO, false));
		if (!duplicates.isEmpty())
			throw new AlreadyExistsException(
					"This Document ID belongs to another employee. Please try with another Document ID.");

		// Validate required data is ok
		if (StringUtils.isNullOrEmpty(newEmployee.getDocId()) || StringUtils.isNullOrEmpty(newEmployee.getFirstName())
				|| StringUtils.isNullOrEmpty(newEmployee.getLastName()) || StringUtils.isNullOrEmpty(newEmployee.getCurrentJobPositionCode()))
			throw new PreconditionFailedException("Please fill all required data and retry.");

		// Generate an entity and map it with DTOs values.
		Employee employeeToAdd = new Employee();
		employeeToAdd = mapper.toEntity(newEmployee);
		JobPosition jobPosition = new JobPosition();
		jobPosition = jobPositionMapper.toEntity(jobPositionService.read(newEmployee.currentJobPositionCode));
		employeeToAdd.setCurrentPosition(jobPosition);
		
		// Save the record and return it to the client
		repository.saveAndFlush(employeeToAdd);
		return read(newEmployee.getDocId());

	}

	/**
	 * Loads the information for a job position given its code.
	 * 
	 * @param documentId the job position code to find.
	 * @throws NotFoundException if no job position is found with given code.
	 */
	@Override
	public EmployeeDTO read(String documentId) throws NotFoundException {
		Optional<Employee> result = repository.findByDocId(documentId);
		if (!result.isPresent())
			throw new NotFoundException("This employee has not been found: " + documentId);
		return mapper.toDTO(result.get());
	}

}
 