package com.cmegroup.sample.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Optional;

import com.cmegroup.sample.data.entities.JobPosition;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JobPositionDTO extends CatalogDTO implements Serializable {

	private static final long serialVersionUID = 5683409528858435360L;

	private String description;

	@NonNull
	private BigDecimal baseSalary;

	public boolean equalsToEntity(JobPosition entity) {

		if (!Optional.ofNullable(entity.getCode()).isPresent() 
				|| !Optional.ofNullable(entity.getName()).isPresent()) {
			return false;
		}

		boolean result = false;

		result = entity.getCode().equalsIgnoreCase(this.getCode());
		result = result && entity.getName().equals(this.getName());
		result = result && entity.getDescription() != null && this.getDescription() != null 
				&& entity.getDescription().equals(this.getDescription());
		result = result && entity.getBaseSalary().equals(this.getBaseSalary());
		result = result && entity.getEnabled().equals(this.getEnabled());
		
		return result;
	}

}
