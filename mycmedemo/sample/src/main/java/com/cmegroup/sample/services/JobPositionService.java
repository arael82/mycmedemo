package com.cmegroup.sample.services;

import java.util.Collection;

import com.cmegroup.sample.dto.JobPositionDTO;
import com.cmegroup.sample.dto.JobPositionSearchDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NoChangeException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.exceptions.PreconditionFailedException;

/**
 * Interface for Job Position Service
 * 
 * @author leonardo
 *
 */
public interface JobPositionService {

	/**
	 * Performs a multiple job position search by criteria.
	 * 
	 * @param searchCriteria a DTO which contains the search criteria provided by the user.
	 * @throws NotFoundException if no job position is found with given criteria.
	 */
	Collection<JobPositionDTO> find(JobPositionSearchDTO searchCriteria) throws NotFoundException;

	/**
	 * <p>
	 * Creates a job position.
	 * </p>
	 * <p>
	 * The following conditions must be fulfilled in order to save successfully:
	 * </p>
	 * <ul>
	 * <li><tt>code</tt> and <tt>name</tt> must be unique</li>
	 * <li><tt>baseSalary</tt> must be more or equals than zero</li>
	 * <li><tt>code</tt> must not be empty</li>
	 * <li><tt>name</tt> must not be empty</li>
	 * </ul>
	 * 
	 * @param newJobPosition the new record to add.
	 * @return a <tt>JobPositionDTO</tt> representing the saved record.
	 * @throws AlreadyExistsException      if some position with the same code or
	 *                                     name exists.
	 * @throws PreconditionFailedException
	 * 
	 */
	JobPositionDTO create(JobPositionDTO newJobPosition) throws AlreadyExistsException, PreconditionFailedException;

	/**
	 * Loads the information for a job position given its code.
	 * 
	 * @param code the job position code to find.
	 * @throws NotFoundException if no job position is found with given code.
	 */
	JobPositionDTO read(String code) throws NotFoundException;

	JobPositionDTO update(JobPositionDTO jobPositionDTO)
			throws NotFoundException, NoChangeException, PreconditionFailedException;

	/**
	 * Deletes a job position given its code.
	 * 
	 * @param code job position code.
	 * @throws NotFoundException           if the record is not found.
	 * @throws PreconditionFailedException if the record is being used by at least
	 *                                     one employee.
	 */
	void delete(String code) throws NotFoundException, PreconditionFailedException;

}
