package com.cmegroup.sample.utils;

import java.util.Random;

import org.springframework.stereotype.Component;

/**
 * 
 * @author leonardo
 *
 */
@Component
public class RandomTextUtil {

	/**
	 * Generates some random string
	 * 
	 * @param length length to generate
	 * @return a string with random characters
	 */
	public static String randomString(long length) {
		Random r = new Random();
		String alphabet = "abcdefghijklABCDEFGHIJKL1234567890";
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < length; i++) {
			result.append(alphabet.charAt(r.nextInt(alphabet.length())));
		} // prints 50 random characters from alphabet

		return result.toString();
	}
}
