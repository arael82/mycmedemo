package com.cmegroup.sample.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeDTO {

	@NonNull
	public Long id;

	@NonNull
	public JobPositionDTO currentPosition;

	@NonNull
	public String docId;

	@NonNull
	public String firstName;

	public String middleName;

	@NonNull
	public String lastName;

}
