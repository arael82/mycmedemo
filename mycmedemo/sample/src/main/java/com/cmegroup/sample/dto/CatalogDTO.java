package com.cmegroup.sample.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class CatalogDTO {

	private String code;

	private String name;

	private Boolean enabled = new Boolean(true);

}
