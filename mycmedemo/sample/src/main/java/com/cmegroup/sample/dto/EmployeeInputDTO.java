package com.cmegroup.sample.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeInputDTO extends EmployeeSearchDTO {

	public EmployeeInputDTO(String docId, String firstName, String middleName, String lastName, String jobPosCode) {
		super(docId, firstName, middleName, lastName);
		this.currentJobPositionCode = jobPosCode;
	}

	public String currentJobPositionCode;

}
