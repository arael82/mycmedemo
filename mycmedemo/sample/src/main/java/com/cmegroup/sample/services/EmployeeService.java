package com.cmegroup.sample.services;

import java.util.Collection;

import com.cmegroup.sample.dto.EmployeeDTO;
import com.cmegroup.sample.dto.EmployeeInputDTO;
import com.cmegroup.sample.dto.EmployeeSearchDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.exceptions.PreconditionFailedException;

public interface EmployeeService {

	/**
	 * List all the employees.
	 * 
	 * @return a collection containing all the employees.
	 * @throws NotFoundException if no employee is found.
	 */
	Collection<EmployeeDTO> find() throws NotFoundException;

	/**
	 * List all the employees by search criteria.
	 * 
	 * @param searchCriteria the search criteria.
	 * @return a collection containing all the employees.
	 * @throws NotFoundException if no employee is found.
	 */
	Collection<EmployeeDTO> find(EmployeeSearchDTO searchCriteria) throws NotFoundException;

	/**
	 * Creates a new Employee.
	 * 
	 * <p>
	 * This operation will be successful if:
	 * <ul>
	 * <li>All required data is provided: First Name, Last Name, Job Position Code,
	 * Document Id.</li>
	 * <li>The document ID is unique.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param newEmployee the information for the new employee
	 * @return the new just-added employee info.
	 * @throws AlreadyExistsException      if an employee with same document id
	 *                                     exists
	 * @throws PreconditionFailedException if required data is not input correctly.
	 */
	EmployeeDTO create(EmployeeInputDTO newEmployee) throws AlreadyExistsException, PreconditionFailedException;

	/**
	 * Loads the information of an employee, given its document id
	 * 
	 * @param documentId Document ID
	 * @return the information of the employee.
	 * @throws NotFoundException if no employee with given ID is found.
	 */
	EmployeeDTO read(String documentId) throws NotFoundException;

}
