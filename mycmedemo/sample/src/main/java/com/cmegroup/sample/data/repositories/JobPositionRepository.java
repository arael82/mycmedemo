package com.cmegroup.sample.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cmegroup.sample.data.entities.JobPosition;

/**
 * JPA Repository for Job Positions
 * 
 * @author leonardo
 *
 */
@Repository
public interface JobPositionRepository
		extends JpaRepository<JobPosition, String>, JpaSpecificationExecutor<JobPosition> {
}
