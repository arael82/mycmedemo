package com.cmegroup.sample.data.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "job_position")
public class JobPosition extends Catalog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5474615308659401572L;
	
	@OneToMany(mappedBy = "currentPosition", fetch = FetchType.LAZY)
	private Collection<Employee> employees = new ArrayList<>();

	@Column(name = "description", length= 255)
	private String description;
	
	@Column(name = "base_salary", nullable = false)
	private BigDecimal baseSalary;

}
