package com.cmegroup.sample.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_MODIFIED)
public class NoChangeException extends RuntimeException {

	private static final long serialVersionUID = -7002333868687884424L;

	public NoChangeException() {
		super();
	}
	
	public NoChangeException(String message) {
		super(message);
	}

	public NoChangeException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoChangeException(Throwable cause) {
		super(cause);
	}

}
