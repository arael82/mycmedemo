package com.cmegroup.sample.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class PreconditionFailedException extends RuntimeException {

	private static final long serialVersionUID = -9032572162100061689L;

	public PreconditionFailedException(String message) {
		super(message);
	}

	public PreconditionFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public PreconditionFailedException(Throwable cause) {
		super(cause);
	}
	
	
	

}
