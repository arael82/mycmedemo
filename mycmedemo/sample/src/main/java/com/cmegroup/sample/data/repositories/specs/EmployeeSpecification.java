package com.cmegroup.sample.data.repositories.specs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.h2.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.cmegroup.sample.data.entities.Employee;
import com.cmegroup.sample.dto.EmployeeSearchDTO;

/**
 * Search specifications for Employees
 * @author leonardo
 *
 */
public class EmployeeSpecification {

	/**
	 * This specification allows to search employees by passing a DTO with all
	 * the search criteria to apply.
	 * 
	 * @param criteria the DTO with search criteria
	 * @return a specification to perform the search through the repository.
	 */
	public static Specification<Employee> hasCriteria(EmployeeSearchDTO criteria, boolean excluding) {
		return (employee, cq, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isNullOrEmpty(criteria.getDocId()))
				predicates.add(cb.equal(employee.get("docId"), criteria.getDocId()));
			if (!StringUtils.isNullOrEmpty(criteria.getFirstName()))
				predicates.add(cb.like(employee.get("firstName"), "%".concat(criteria.getFirstName()).concat("%")));
			if (!StringUtils.isNullOrEmpty(criteria.getMiddleName()))
				predicates.add(cb.like(employee.get("middleName"), "%".concat(criteria.getMiddleName()).concat("%")));
			if (!StringUtils.isNullOrEmpty(criteria.getLastName()))
				predicates.add(cb.like(employee.get("lastName"), "%".concat(criteria.getLastName()).concat("%")));

			Predicate[] p = predicates.toArray(new Predicate[predicates.size()]);

			return p.length == 0 ? null : 
				p.length == 1 ? p[0] : 
					excluding ? cb.and(p) : cb.or(p);
		};
	}

}
