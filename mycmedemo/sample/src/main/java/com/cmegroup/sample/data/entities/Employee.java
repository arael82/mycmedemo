package com.cmegroup.sample.data.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "position", nullable = false)
	public JobPosition currentPosition;

	@Column(name = "doc_id", length = 20, unique = true, nullable = false)
	public String docId;

	@Column(name = "first_name", length = 50, nullable = false)
	public String firstName;

	@Column(name = "mid_name", length = 50)
	public String middleName;

	@Column(name = "last_name", length = 50, nullable = false)
	public String lastName;

}
