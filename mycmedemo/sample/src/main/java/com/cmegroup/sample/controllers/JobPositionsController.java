package com.cmegroup.sample.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cmegroup.sample.controllers.wrappers.JobPositionWrapper;
import com.cmegroup.sample.dto.JobPositionDTO;
import com.cmegroup.sample.dto.JobPositionSearchDTO;
import com.cmegroup.sample.services.JobPositionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/job-positions")
@Slf4j
public class JobPositionsController {

	@Autowired
	private JobPositionService service;

	@RequestMapping(value = "search", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Collection<JobPositionDTO>> find(@RequestBody JobPositionSearchDTO searchCriteria) {
		return new ResponseEntity<Collection<JobPositionDTO>>(service.find(searchCriteria), HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<JobPositionDTO> create(@RequestBody JobPositionWrapper wrapper) {
		log.info("User has requested creation of record: job position");
		return new ResponseEntity<JobPositionDTO>(service.create(wrapper.getJobPosition()), HttpStatus.OK);
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<JobPositionDTO> read(@PathVariable(name="code", required = true) String code) {
		return new ResponseEntity<JobPositionDTO>(service.read(code), HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<JobPositionDTO> update(@RequestBody JobPositionWrapper wrapper) {
		log.info("User has requested update of record: job position - " + wrapper.getJobPosition().getCode());
		return new ResponseEntity<JobPositionDTO>(service.update(wrapper.getJobPosition()), HttpStatus.OK);
	}

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/{code}", method = RequestMethod.DELETE)
	public void delete(@PathVariable(name="code", required = true) String code) {
		log.info("User has requested deletion of record: job position - " + code);
		service.delete(code);
	}

}
