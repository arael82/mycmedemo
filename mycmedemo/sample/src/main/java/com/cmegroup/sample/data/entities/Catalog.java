package com.cmegroup.sample.data.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class Catalog implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7980475822129888461L;

	@Id
	@Column(name = "code", length = 4)
	private String code;

	@Column(name = "name", length = 50, nullable = false, unique = true)
	private String name;

	@Column(name = "is_enabled", nullable = false)
	private Boolean enabled = new Boolean(true);

}
