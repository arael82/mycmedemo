package com.cmegroup.sample.data.repositories.specs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.h2.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.dto.JobPositionSearchDTO;

/**
 * Search specifications for Job Positions
 * @author leonardo
 *
 */
public class JobPositionSpecification {

	/**
	 * This specification allows to search job positions by passing a DTO with all
	 * the search criteria to apply.
	 * 
	 * @param criteria the DTO with search criteria
	 * @return a specification to perform the search through the repository.
	 */
	public static Specification<JobPosition> hasCriteria(JobPositionSearchDTO criteria, boolean excluding) {
		return (jobPosition, cq, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isNullOrEmpty(criteria.getCode()))
				predicates.add(cb.equal(jobPosition.get("code"), criteria.getCode()));
			if (!StringUtils.isNullOrEmpty(criteria.getName()))
				predicates.add(cb.like(jobPosition.get("name"), "%".concat(criteria.getName()).concat("%")));

			boolean searchSalaryFrom = Optional.ofNullable(criteria.getBaseSalaryFrom()).isPresent()
					&& criteria.getBaseSalaryFrom().compareTo(BigDecimal.ZERO) >= 0;
			boolean searchSalaryTo = Optional.ofNullable(criteria.getBaseSalaryTo()).isPresent()
					&& criteria.getBaseSalaryTo().compareTo(BigDecimal.ZERO) >= 0;

			if (searchSalaryFrom && searchSalaryTo) {
				predicates.add(cb.between(jobPosition.get("baseSalary"), criteria.getBaseSalaryFrom(),
						criteria.getBaseSalaryTo()));
			} else if (searchSalaryFrom && !searchSalaryTo) {
				predicates.add(cb.equal(jobPosition.get("baseSalary"), criteria.getBaseSalaryFrom()));
			} else if (!searchSalaryFrom && searchSalaryTo) {
				predicates.add(cb.equal(jobPosition.get("baseSalary"), criteria.getBaseSalaryTo()));
			}

			Predicate[] p = predicates.toArray(new Predicate[predicates.size()]);

			return p.length == 0 ? null : 
				p.length == 1 ? p[0] : 
					excluding ? cb.and(p) : cb.or(p);
		};
	}

}
