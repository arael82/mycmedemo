package com.cmegroup.sample.controllers.wrappers;

import java.io.Serializable;

import com.cmegroup.sample.dto.JobPositionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobPositionWrapper implements Serializable {

	private static final long serialVersionUID = 5168990057225669574L;
	private JobPositionDTO jobPosition = new JobPositionDTO();

}
