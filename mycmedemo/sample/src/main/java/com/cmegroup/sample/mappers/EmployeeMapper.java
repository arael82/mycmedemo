package com.cmegroup.sample.mappers;

import java.util.Collection;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.cmegroup.sample.data.entities.Employee;
import com.cmegroup.sample.dto.EmployeeDTO;
import com.cmegroup.sample.dto.EmployeeInputDTO;

@Mapper(componentModel = "spring", uses = JobPositionMapper.class)
public interface EmployeeMapper {

	EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

	EmployeeDTO toDTO(Employee source);

	Employee toEntity(EmployeeDTO source);

	Employee toEntity(EmployeeInputDTO source);

	Collection<EmployeeDTO> toDTOs(Collection<Employee> source);

	Collection<Employee> toEntities(Collection<EmployeeDTO> source);

}
