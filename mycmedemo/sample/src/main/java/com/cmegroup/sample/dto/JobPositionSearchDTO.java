package com.cmegroup.sample.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobPositionSearchDTO extends CatalogDTO implements Serializable {

	private static final long serialVersionUID = 5337735945201792439L;

	private BigDecimal baseSalaryFrom;
	private BigDecimal baseSalaryTo;

}
