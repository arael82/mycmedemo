package com.cmegroup.sample.services.impl;

import java.util.Collection;
import java.util.Optional;

import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cmegroup.sample.data.entities.JobPosition;
import com.cmegroup.sample.data.repositories.JobPositionRepository;
import com.cmegroup.sample.data.repositories.specs.JobPositionSpecification;
import com.cmegroup.sample.dto.JobPositionDTO;
import com.cmegroup.sample.dto.JobPositionSearchDTO;
import com.cmegroup.sample.exceptions.AlreadyExistsException;
import com.cmegroup.sample.exceptions.NoChangeException;
import com.cmegroup.sample.exceptions.NotFoundException;
import com.cmegroup.sample.exceptions.PreconditionFailedException;
import com.cmegroup.sample.mappers.JobPositionMapper;
import com.cmegroup.sample.services.JobPositionService;

/**
 * Services and main business logic for Job Position Service
 * 
 * @author leonardo
 *
 */
@Service
public class JobPositionServiceImpl implements JobPositionService {

	@Autowired
	private JobPositionRepository repository;
	
	@Autowired
	private JobPositionMapper mapper;

	/**
	 * Performs a multiple job position search by criteria.
	 * 
	 * @param searchCriteria a DTO which contains the search criteria that the user
	 *                       provided.
	 * @throws NotFoundException if no job position is found with given criteria.
	 */
	@Override
	public Collection<JobPositionDTO> find(JobPositionSearchDTO searchCriteria) throws NotFoundException {
		Collection<JobPosition> result = repository.findAll(JobPositionSpecification.hasCriteria(searchCriteria, true));
		if (result.isEmpty())
			throw new NotFoundException("No jobs position with the specified search criteria");
		return mapper.toDTOs(result);
	}

	/**
	 * <p>
	 * Creates a job position.
	 * </p>
	 * <p>
	 * The following conditions must be fulfilled in order to save successfully:
	 * </p>
	 * <ul>
	 * <li><tt>code</tt> and <tt>name</tt> must be unique</li>
	 * <li><tt>baseSalary</tt> must be more or equals than zero</li>
	 * <li><tt>code</tt> must not be empty</li>
	 * <li><tt>name</tt> must not be empty</li>
	 * </ul>
	 * 
	 * @param newJobPosition the new record to add.
	 * @return a <tt>JobPositionDTO</tt> representing the saved record.
	 * @throws AlreadyExistsException      if some position with the same code or
	 *                                     name exists.
	 * @throws PreconditionFailedException
	 * 
	 */
	@Override
	public JobPositionDTO create(JobPositionDTO newJobPosition)
			throws AlreadyExistsException, PreconditionFailedException {

		// Avoid duplicate data
		JobPositionSearchDTO searchDTO = new JobPositionSearchDTO();
		searchDTO.setCode(newJobPosition.getCode());
		searchDTO.setName(newJobPosition.getName());
		Collection<JobPosition> duplicates = repository.findAll(JobPositionSpecification.hasCriteria(searchDTO, false));
		if (!duplicates.isEmpty())
			throw new AlreadyExistsException(
					"At least one job position exists with the code and/or name that you specified.");

		// Validate required data is ok
		if (StringUtils.isNullOrEmpty(newJobPosition.getCode()) || StringUtils.isNullOrEmpty(newJobPosition.getName()))
			throw new PreconditionFailedException("You must specify a valid code and name for this position.");

		// Generate an entity and map it with DTOs values.
		JobPosition jobPosition = new JobPosition();
		jobPosition = mapper.toEntity(newJobPosition);

		// Save the record and return it to the client
		repository.saveAndFlush(jobPosition);
		return mapper.toDTO(jobPosition);

	}

	/**
	 * Loads the information for a job position given its code.
	 * 
	 * @param code the job position code to find.
	 * @throws NotFoundException if no job position is found with given code.
	 */
	@Override
	public JobPositionDTO read(String code) throws NotFoundException {
		Optional<JobPosition> result = repository.findById(code);
		if (!result.isPresent())
			throw new NotFoundException("This job position has not been found: " + code);
		return mapper.toDTO(result.get());
	}

	/**
	 * <p>
	 * Updates an existing job position.
	 * </p>
	 * <p>
	 * The following conditions must be fulfilled in order to save successfully:
	 * </p>
	 * <ul>
	 * <li><tt>baseSalary</tt> must be more or equals than zero</li>
	 * <li><tt>code</tt> must not be empty</li>
	 * <li><tt>name</tt> must not be empty</li>
	 * </ul>
	 * *
	 * 
	 * @param jobPositionDTO a DTO containing the new values to save.
	 * @throws NotFoundException           if the job position does not exist.
	 * @throws NoChangeException           if the data was sent to the service with
	 *                                     no change.
	 * @throws PreconditionFailedException if some condition could not be
	 *                                     accomplished to save the record.
	 */
	@Override
	@Transactional
	public JobPositionDTO update(JobPositionDTO jobPositionDTO)
			throws NotFoundException, NoChangeException, PreconditionFailedException {
		Optional<JobPosition> jobPosition = repository.findById(jobPositionDTO.getCode());
		if (!jobPosition.isPresent())
			throw new NotFoundException("This job position has not been found: " + jobPositionDTO.getCode());
		if (jobPositionDTO.equalsToEntity(jobPosition.get()))
			throw new NoChangeException();
		// Validate required data is ok
		if (StringUtils.isNullOrEmpty(jobPositionDTO.getCode()) || StringUtils.isNullOrEmpty(jobPositionDTO.getName()))
			throw new PreconditionFailedException("You must specify a valid code and name for this position.");

		// Generate an entity and map it with DTOs values.
		jobPosition.get().setCode(jobPositionDTO.getCode());
		jobPosition.get().setName(jobPositionDTO.getName());
		jobPosition.get().setDescription(jobPositionDTO.getDescription());
		jobPosition.get().setBaseSalary(jobPositionDTO.getBaseSalary());
		jobPosition.get().setEnabled(jobPositionDTO.getEnabled());

		// Save the record and return it to the client
		repository.saveAndFlush(jobPosition.get());
		return mapper.toDTO(jobPosition.get());

	}

	/**
	 * Deletes a job position given its code.
	 * 
	 * @param code job position code.
	 * @throws NotFoundException           if the record is not found.
	 * @throws PreconditionFailedException if the record is being used by at least
	 *                                     one employee.
	 */
	@Override
	@Transactional
	public void delete(String code) throws NotFoundException, PreconditionFailedException {
		Optional<JobPosition> jobPosition = repository.findById(code);
		// Validate that the position exists
		if (!jobPosition.isPresent())
			throw new NotFoundException("This job position has not been found: " + code);
		// Validate that no employee is using the position to delete.
		if (!jobPosition.get().getEmployees().isEmpty())
			throw new PreconditionFailedException("This position cannot be deleted as it is in use");
		// Perform deletion
		repository.deleteById(code);
	}

}
